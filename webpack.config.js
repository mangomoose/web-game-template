const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'js/game.js',
  },
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader', exclude: '/node_modules/' }],
  },
  devServer: {
    watchContentBase: true,
    contentBase: path.resolve(__dirname, '/build'),
    host: '127.0.0.1',
    port: 8080,
    open: true,
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'assets', to: 'assets' }, { from: 'web' }],
    }),
  ],
};
