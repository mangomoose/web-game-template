# Requirements

1. Node.js - https://nodejs.org/en/download/ You will need Node.js version 12 or higher installed on your machine.
2. Terminal - You will need access to a command line terminal so your can run the project commands.

# Install dependencies

`npm install` - Run this command from the root of the project to install the project dependencies. You will need to run this at least once before you can use the development or production commands.

# Development

`npm run dev` - To work on the project locally you can start up the Webpack dev server by running this command from the root of the project. This will open the project in your browser and update when any files are saved allowing you to quickly iterate on your ideas.

Make sure you have installed the project dependencies before using this command.

# Production

`npm run build` - To produce a production build you can run this command from the root of the project. This will assemble all the code and assets into the `build` folder which you can upload wherever you like.

Make sure you have installed the project dependencies before using this command.

# Notes

All graphical and audio assets used in this project are free to use.
