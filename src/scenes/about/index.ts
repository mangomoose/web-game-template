import { GAME_CONFIG, FONT } from '../../config';
import { createButton } from '../../utils/ui';

export class AboutScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'AboutScene',
    });
  }

  init(): void {}

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;
    const canvasHalfWidth = canvasWidth / 2;
    const canvasHalfHeight = canvasHeight / 2;

    const titleText = this.add.text(canvasWidth / 2, 100, 'ABOUT', {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    titleText.setOrigin(0.5);

    const backButtonX = 120;
    const backButtonY = canvasHeight - 50;
    createButton(this, backButtonX, backButtonY, 'medium', 'rectangle', 'BACK', () => {
      this.scene.start('MenuScene');
    });

    const aboutText = this.add.text(canvasWidth / 2, 300, 'Developed by Mango Moose.', {
      fontFamily: FONT,
      fontSize: 28,
      color: '#ffffff',
      align: 'center',
      wordWrap: { width: 700 },
      stroke: '#000000',
      strokeThickness: 4,
    });
    aboutText.setOrigin(0.5);

    const websiteButtonX = canvasWidth / 2 - 125;
    const websiteButtonY = 400;
    createButton(this, websiteButtonX, websiteButtonY, 'medium', 'rectangle', 'WEBSITE', () => {
      window.open('https://mangomoose.co.uk/', '_blank');
    });

    const itchButtonX = canvasWidth / 2 + 125;
    const itchButtonY = 400;
    createButton(this, itchButtonX, itchButtonY, 'medium', 'rectangle', 'ITCH.IO', () => {
      window.open('https://mango-moose.itch.io/', '_blank');
    });

    const versionText = this.add.text(canvasWidth - 60, canvasHeight - 30, GAME_CONFIG.version, {
      fontFamily: FONT,
      fontSize: 20,
      color: '#ffffff',
    });
    versionText.setOrigin(0.5);
  }

  update(): void {}
}
