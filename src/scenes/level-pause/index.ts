import { FONT } from '../../config';
import { createButton } from '../../utils/ui';

export class LevelPauseScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'LevelPauseScene',
    });
  }

  init(): void {}

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;
    const canvasHalfWidth = canvasWidth / 2;
    const canvasHalfHeight = canvasHeight / 2;
    const buttonStart = canvasHeight / 2;

    const overlay = this.add.graphics();
    overlay.fillStyle(0x000000, 0.75);
    overlay.fillRect(0, 0, canvasWidth, canvasHeight);
    overlay.setInteractive({
      hitArea: new Phaser.Geom.Rectangle(0, 0, canvasWidth, canvasHeight),
      hitAreaCallback: Phaser.Geom.Rectangle.Contains,
    });

    const panel = this.add.image(canvasHalfWidth, canvasHalfHeight, 'uiPanel');
    panel.setScale(1.5, 1);

    const titleText = this.add.text(canvasWidth / 2, 200, 'Paused', {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    titleText.setOrigin(0.5);

    createButton(this, canvasWidth / 2, buttonStart, 'medium', 'rectangle', 'RESUME', () => {
      this.scene.stop();
      this.scene.resume('LevelScene');
    });
    createButton(this, canvasWidth / 2, buttonStart + 100, 'medium', 'rectangle', 'MENU', () => {
      this.scene.stop();
      this.scene.stop('LevelScene');
      this.scene.start('MenuScene');
    });
  }

  update(): void {}
}
