import { GAME_CONFIG, FONT } from '../../config';
import { createButton } from '../../utils/ui';
import { getMusicVolume, setMusicVolume, getSFXVolume, setSFXVolume } from '../../utils/sound';

export class OptionsScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'OptionsScene',
    });
  }

  init(): void {}

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;

    const titleText = this.add.text(canvasWidth / 2, 100, 'OPTIONS', {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    titleText.setOrigin(0.5);

    const musicLabelText = this.add.text(canvasWidth / 2.5, 300, 'Music volume:', {
      fontFamily: FONT,
      fontSize: 28,
      color: '#ffffff',
      align: 'center',
      stroke: '#000000',
      strokeThickness: 4,
    });
    musicLabelText.setOrigin(1, 0.5);

    const musicValueTest = this.add.text(900, 300, `${Math.floor(getMusicVolume() * 100)}%`, {
      fontFamily: FONT,
      fontSize: 28,
      color: '#ffffff',
      align: 'center',
      stroke: '#000000',
      strokeThickness: 4,
    });
    musicValueTest.setOrigin(0.5);

    const musicSliderStartX = 500;
    const musicSliderEndX = 800;
    const musicSliderRange = musicSliderEndX - musicSliderStartX;

    const musicSliderBar = this.add.image(musicSliderStartX - 16, 300, 'uiSliderBar');
    musicSliderBar.setOrigin(0, 0.5);

    const musicSlider = this.add.image(
      musicSliderStartX + musicSliderRange * getMusicVolume(),
      295,
      'uiSlider',
    );
    musicSlider.setInteractive();
    musicSlider.setOrigin(0.5, 0);
    musicSlider.on('drag', (gameObject: Phaser.GameObjects.GameObject, dragX: number) => {
      const cappedDragX = Math.max(musicSliderStartX, Math.min(musicSliderEndX, dragX));
      musicSlider.x = cappedDragX;
      setMusicVolume((cappedDragX - musicSliderStartX) / musicSliderRange);
      musicValueTest.text = `${Math.floor(getMusicVolume() * 100)}%`;
    });
    musicSlider.on('dragend', () => {});
    this.input.setDraggable(musicSlider);

    const sfxText = this.add.text(canvasWidth / 2.5, 400, 'SFX volume:', {
      fontFamily: FONT,
      fontSize: 28,
      color: '#ffffff',
      align: 'center',
      stroke: '#000000',
      strokeThickness: 4,
      textAlign: 'right',
    });
    sfxText.setOrigin(1, 0.5);

    const sfxValueTest = this.add.text(900, 400, `${Math.floor(getSFXVolume() * 100)}%`, {
      fontFamily: FONT,
      fontSize: 28,
      color: '#ffffff',
      align: 'center',
      stroke: '#000000',
      strokeThickness: 4,
    });
    sfxValueTest.setOrigin(0.5);

    const sfxSliderStartX = 500;
    const sfxSliderEndX = 800;
    const sfxSliderRange = sfxSliderEndX - sfxSliderStartX;

    const sfxSliderBar = this.add.image(sfxSliderStartX - 16, 400, 'uiSliderBar');
    sfxSliderBar.setOrigin(0, 0.5);

    const sfxSlider = this.add.image(
      sfxSliderStartX + sfxSliderRange * getSFXVolume(),
      395,
      'uiSlider',
    );
    sfxSlider.setInteractive();
    sfxSlider.setOrigin(0.5, 0);
    sfxSlider.on('drag', (gameObject: Phaser.GameObjects.GameObject, dragX: number) => {
      const cappedDragX = Math.max(sfxSliderStartX, Math.min(sfxSliderEndX, dragX));
      sfxSlider.x = cappedDragX;
      setSFXVolume((cappedDragX - sfxSliderStartX) / sfxSliderRange);
      sfxValueTest.text = `${Math.floor(getSFXVolume() * 100)}%`;
    });
    sfxSlider.on('dragend', () => {});
    this.input.setDraggable(sfxSlider);

    const backButtonX = 120;
    const backButtonY = canvasHeight - 50;
    createButton(this, backButtonX, backButtonY, 'medium', 'rectangle', 'BACK', () => {
      this.scene.start('MenuScene');
    });

    const versionText = this.add.text(canvasWidth - 60, canvasHeight - 30, GAME_CONFIG.version, {
      fontFamily: FONT,
      fontSize: 20,
      color: '#ffffff',
    });
    versionText.setOrigin(0.5);
  }

  update(): void {}
}
