import { FONT, LEVEL_UNLOCKED_KEY, TUTORIAL_CONFIG, LEVEL_CONFIG, LevelConfig } from '../../config';
import { createButton } from '../../utils/ui';

export class LevelEndScene extends Phaser.Scene {
  private resultsData: {
    win: boolean;
    levelConfig: LevelConfig;
  };

  constructor() {
    super({
      key: 'LevelEndScene',
    });
  }

  init(resultsData: {
    win: boolean;
    levelConfig: LevelConfig;
  }): void {
    this.resultsData = resultsData;

    if (this.resultsData.levelConfig !== TUTORIAL_CONFIG && this.resultsData.win) {
      const unlockedLevel = window.localStorage.getItem(LEVEL_UNLOCKED_KEY) || 0;
      const levelIndex = LEVEL_CONFIG.findIndex(
        (element) => element === this.resultsData.levelConfig,
      );
      if (levelIndex >= unlockedLevel) {
        window.localStorage.setItem(LEVEL_UNLOCKED_KEY, `${levelIndex + 1}`);
      }
    }
  }

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;
    const canvasHalfWidth = canvasWidth / 2;
    const canvasHalfHeight = canvasHeight / 2;

    const overlay = this.add.graphics();
    overlay.fillStyle(0x000000, 0.75);
    overlay.fillRect(0, 0, canvasWidth, canvasHeight);
    overlay.setInteractive({
      hitArea: new Phaser.Geom.Rectangle(0, 0, canvasWidth, canvasHeight),
      hitAreaCallback: Phaser.Geom.Rectangle.Contains,
    });

    const panel = this.add.image(canvasHalfWidth, canvasHalfHeight, 'uiPanel');
    panel.setScale(1.5, 1);

    const resultTextData = this.resultsData.win ? 'VICTORY' : 'DEFEAT';
    const resultText = this.add.text(canvasWidth / 2, 200, resultTextData, {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    resultText.setOrigin(0.5);

    if (this.resultsData.win) {
      createButton(this, canvasWidth / 2, 525, 'medium', 'rectangle', 'CONTINUE', () => {
        this.scene.stop();
        this.scene.stop('LevelScene');
        this.scene.start('LevelSelectScene');
      });
    } else {
      createButton(this, canvasWidth / 2 + 150, 525, 'medium', 'rectangle', 'CONTINUE', () => {
        this.scene.stop();
        this.scene.stop('LevelScene');
        this.scene.start('LevelSelectScene');
      });
      createButton(this, canvasWidth / 2 - 150, 525, 'medium', 'rectangle', 'RETRY', () => {
        this.scene.stop();
        this.scene.stop('LevelScene');
        this.scene.launch('LevelScene', { levelConfig: this.resultsData.levelConfig });
      });
    }
  }

  update(): void {}
}
