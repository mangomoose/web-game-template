import { LevelConfig } from '../../config';
import { createButton } from '../../utils/ui';

export class LevelScene extends Phaser.Scene {
  private levelConfig: LevelConfig;
  private won: boolean = false;
  private lost: boolean = false;

  constructor() {
    super({
      key: 'LevelScene',
    });
  }

  init(data: { levelConfig: LevelConfig }): void {
    this.levelConfig = data.levelConfig;
    this.won = false;
    this.lost = false;
  }

  create(): void {
    const fullWidth = this.scene.systems.canvas.width;

    createButton(this, fullWidth / 2 + 150, 600, 'medium', 'rectangle', 'WIN', () => {
      this.won = true;
    });
    createButton(this, fullWidth / 2 - 150, 600, 'medium', 'rectangle', 'LOSE', () => {
      this.lost = true;
    });

    this.input.keyboard.on('keydown_ESC', () => {
      this.scene.pause();
      this.scene.launch('LevelPauseScene');
    });
  }

  update(): void {
    if (this.won) {
      this.scene.pause();
      this.scene.launch('LevelEndScene',{
        win: true,
        levelConfig: this.levelConfig,
      });
    }

    if (this.lost) {
      this.scene.pause();
      this.scene.launch('LevelEndScene', {
        win: false,
        levelConfig: this.levelConfig,
      });
    }
  }
}
