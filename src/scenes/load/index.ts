import { COLOUR_PRIMARY, COLOUR_SECONDARY } from '../../config';

export class LoadScene extends Phaser.Scene {
  private assetPackData: Array<Phaser.Types.Loader.FileTypes.PackFileConfig>;
  private barBackground: Phaser.GameObjects.Graphics;
  private bar: Phaser.GameObjects.Graphics;
  private loadBarWidth: number = 300;
  private loadBarHeight: number = 38;
  private loadBarBorder: number = 4;
  private loadBarHorizontalOffset: number = 0;
  private loadBarVerticalOffset: number = 100;

  constructor() {
    super({
      key: 'LoadScene',
    });
  }

  init(assetPackData: Array<Phaser.Types.Loader.FileTypes.PackFileConfig>): void {
    this.assetPackData = assetPackData;

    const canvasHalfWidth = this.scene.systems.canvas.width / 2;
    const canvasHalfHeight = this.scene.systems.canvas.height / 2;

    this.barBackground = this.add.graphics();
    this.barBackground.fillStyle(COLOUR_PRIMARY, 1);
    this.barBackground.fillRect(
      canvasHalfWidth - this.loadBarWidth / 2 + this.loadBarHorizontalOffset,
      canvasHalfHeight - this.loadBarHeight / 2 + this.loadBarVerticalOffset,
      this.loadBarWidth,
      this.loadBarHeight,
    );
    this.bar = this.add.graphics();
    this.bar.fillStyle(COLOUR_SECONDARY, 1);
  }

  preload(): void {
    this.load.on('progress', this.handleLoadProgress, this);
    this.load.on('complete', this.handleLoadComplete, this);
    this.load.pack(this.assetPackData);
  }

  handleLoadProgress(value: number): void {
    const canvasHalfWidth = this.scene.systems.canvas.width / 2;
    const canvasHalfHeight = this.scene.systems.canvas.height / 2;

    const loadBarWidth = this.loadBarWidth - this.loadBarBorder * 2;
    const loadBarHeight = this.loadBarHeight - this.loadBarBorder * 2;
    this.bar.fillRect(
      canvasHalfWidth - loadBarWidth / 2 + this.loadBarHorizontalOffset,
      canvasHalfHeight - loadBarHeight / 2 + this.loadBarVerticalOffset,
      loadBarWidth * value,
      loadBarHeight,
    );
  }

  handleLoadComplete(): void {
    const canvasHalfWidth = this.scene.systems.canvas.width / 2;
    const canvasHalfHeight = this.scene.systems.canvas.height / 2;
    const logo = this.add.image(canvasHalfWidth, canvasHalfHeight - 100, 'uiLogo');
    logo.setScale(1.5);
  }

  create(): void {}

  update(): void {
    this.time.delayedCall(1500, () => {
      this.scene.start('MenuScene');
    });
  }
}
