import { FONT, LevelConfig } from '../../config';
import { createButton } from '../../utils/ui';

export class LevelBriefScene extends Phaser.Scene {
  private levelConfig: LevelConfig;

  constructor() {
    super({
      key: 'LevelBriefScene',
    });
  }

  init(data: { levelConfig: LevelConfig }): void {
    this.levelConfig = data.levelConfig;
  }

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;

    const titleText = this.add.text(canvasWidth / 2, 100, `LEVEL: ${this.levelConfig.name}`, {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    titleText.setOrigin(0.5);

    const backButtonX = 120;
    const backButtonY = canvasHeight - 50;
    createButton(this, backButtonX, backButtonY, 'medium', 'rectangle', 'BACK', () => {
      this.scene.start('LevelSelectScene');
    });

    const startButtonX = canvasWidth - 120;
    const startButtonY = canvasHeight - 50;
    createButton(this, startButtonX, startButtonY, 'medium', 'rectangle', 'START', () => {
      this.scene.start('LevelScene', { levelConfig: this.levelConfig });
    });

    const levelText = this.add.text(canvasWidth / 2, 250, this.levelConfig.description, {
      fontFamily: FONT,
      fontSize: 30,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 4,
      align: 'center',
      wordWrap: { width: 800 },
    });
    levelText.setOrigin(0.5);
  }

  update(): void {}
}
