import { GAME_CONFIG, FONT } from '../../config';
import { createButton } from '../../utils/ui';

export class MenuScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'MenuScene',
    });
  }

  init(): void {}

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;
    const canvasHalfWidth = canvasWidth / 2;
    const buttonStart = 300;
    const buttonOffset = 100;

    const titleText = this.add.text(canvasHalfWidth, 100, GAME_CONFIG.title, {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    titleText.setOrigin(0.5);

    createButton(
      this,
      canvasHalfWidth,
      buttonStart + buttonOffset * 0,
      'large',
      'rectangle',
      'PLAY',
      () => {
        this.scene.start('LevelSelectScene');
      },
    );
    createButton(
      this,
      canvasHalfWidth,
      buttonStart + buttonOffset * 1,
      'large',
      'rectangle',
      'OPTIONS',
      () => {
        this.scene.start('OptionsScene');
      },
    );
    createButton(
      this,
      canvasHalfWidth,
      buttonStart + buttonOffset * 2,
      'large',
      'rectangle',
      'ABOUT',
      () => {
        this.scene.start('AboutScene');
      },
    );

    const versionText = this.add.text(canvasWidth - 60, canvasHeight - 30, GAME_CONFIG.version, {
      fontFamily: FONT,
      fontSize: 20,
      color: '#ffffff',
    });
    versionText.setOrigin(0.5);
  }

  update(): void {}
}
