import { GAME_CONFIG, FONT, LEVEL_UNLOCKED_KEY, TUTORIAL_CONFIG, LEVEL_CONFIG } from '../../config';
import { createButton } from '../../utils/ui';

export class LevelSelectScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'LevelSelectScene',
    });
  }

  init(): void {}

  create(): void {
    this.input.setDefaultCursor('url(assets/cursors/hand.cur), pointer');

    const canvasWidth = this.scene.systems.canvas.width;
    const canvasHeight = this.scene.systems.canvas.height;

    const titleText = this.add.text(canvasWidth / 2, 100, 'LEVEL SELECT', {
      fontFamily: FONT,
      fontSize: 60,
      color: '#ffffff',
      stroke: '#000000',
      strokeThickness: 6,
    });
    titleText.setOrigin(0.5);

    const tutorialButtonX = canvasWidth / 2;
    const tutorialButtonY = 260;

    createButton(this, tutorialButtonX, tutorialButtonY, 'large', 'rectangle', 'TUTORIAL', () => {
      this.scene.start('LevelBriefScene', { levelConfig: TUTORIAL_CONFIG });
    });

    const levelUnlocked = window.localStorage.getItem(LEVEL_UNLOCKED_KEY) || 0;
    const rowSpacing = 100;
    const columnSpacing = 100;
    const columnsPerRow = 5;
    const columnOffset = -Math.floor(columnsPerRow / 2);
    let positionIndex = 0;
    for (let levelIndex = 0; levelIndex < LEVEL_CONFIG.length; levelIndex++) {
      const row = Math.floor(positionIndex / columnsPerRow);
      const column = positionIndex % columnsPerRow;
      const y = canvasHeight / 2 + 30 + row * rowSpacing;
      const x = canvasWidth / 2 + (column + columnOffset) * columnSpacing;
      const unlocked = levelUnlocked >= levelIndex;

      const [buttonText, button] = createButton(
        this,
        x,
        y,
        'large',
        'square',
        `${levelIndex + 1}`,
        () => {
          this.scene.start('LevelBriefScene', { levelConfig: LEVEL_CONFIG[levelIndex] });
        },
        unlocked,
      );

      if (!unlocked) {
        button.setAlpha(0.5);
        buttonText.setAlpha(0.5);
      }

      positionIndex++;
    }

    const versionText = this.add.text(canvasWidth - 60, canvasHeight - 30, GAME_CONFIG.version, {
      fontFamily: FONT,
      fontSize: 20,
      color: '#ffffff',
    });
    versionText.setOrigin(0.5);

    const backButtonX = 120;
    const backButtonY = canvasHeight - 50;
    createButton(this, backButtonX, backButtonY, 'medium', 'rectangle', 'BACK', () => {
      this.scene.start('MenuScene');
    });
  }

  update(): void {}
}
