export const GAME_CONFIG: Phaser.Types.Core.GameConfig = {
  title: 'Game',
  version: 'v1.1.0',
  width: 1080,
  height: 720,
  scale: {
    parent: 'game',
    mode: Phaser.Scale.FIT,
    width: 1080,
    height: 720,
  },
  zoom: 1,
  type: Phaser.WEBGL,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 0 },
      debug: false,
    },
  },
  backgroundColor: '#000000',
};

export const FONT = 'RussoOne';
export const COLOUR_PRIMARY = 0xff7d0a;
export const COLOUR_SECONDARY = 0xffc20a;
export const COLOUR_PRIMARY_HTML = '#ff7d0a';
export const COLOUR_SECONDARY_HTML = '#ffc20a';

export const LEVEL_UNLOCKED_KEY = `wtb_${GAME_CONFIG.version}_unlocked_level`;

export interface LevelConfig {
  name: string;
  description: string;
}

export const TUTORIAL_CONFIG: LevelConfig = {
  name: 'TUTORIAL',
  description: '',
};

export const LEVEL_CONFIG: Array<LevelConfig> = [
  {
    name: 'Level 1',
    description: '',
  },
  {
    name: 'Level 2',
    description: '',
  },
  {
    name: 'Level 3',
    description: '',
  },
  {
    name: 'Level 4',
    description: '',
  },
  {
    name: 'Level 5',
    description: '',
  },
  {
    name: 'Level 6',
    description: '',
  },
  {
    name: 'Level 7',
    description: '',
  },
  {
    name: 'Level 8',
    description: '',
  },
  {
    name: 'Level 9',
    description: '',
  },
  {
    name: 'Level 10',
    description: '',
  },
];
