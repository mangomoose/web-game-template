import * as Phaser from 'phaser';

import { LoadScene } from './scenes/load';
import { MenuScene } from './scenes/menu';
import { OptionsScene } from './scenes/options';
import { AboutScene } from './scenes/about';
import { LevelSelectScene } from './scenes/level-select';
import { LevelBriefScene } from './scenes/level-brief';
import { LevelScene } from './scenes/level';
import { LevelPauseScene } from './scenes/level-pause';
import { LevelEndScene } from './scenes/level-end';

import { GAME_CONFIG } from './config';

export class Game extends Phaser.Game {
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
  }

  start() {
    super.start();

    this.scene.add('LoadScene', LoadScene);
    this.scene.add('MenuScene', MenuScene);
    this.scene.add('OptionsScene', OptionsScene);
    this.scene.add('AboutScene', AboutScene);
    this.scene.add('LevelSelectScene', LevelSelectScene);
    this.scene.add('LevelBriefScene', LevelBriefScene);
    this.scene.add('LevelScene', LevelScene);
    this.scene.add('LevelPauseScene', LevelPauseScene);
    this.scene.add('LevelEndScene', LevelEndScene);

    this.scene.start('LoadScene', [
      { key: 'audio', url: 'assets/packs/audio.json' },
      { key: 'image', url: 'assets/packs/image.json' },
      { key: 'spritesheet', url: 'assets/packs/spritesheet.json' },
    ]);
  }
}

new Game(GAME_CONFIG);
