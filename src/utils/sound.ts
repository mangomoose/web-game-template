let musicVolume = 1.0;
let sfxVolume = 1.0;

export const getMusicVolume = (): number => {
  return musicVolume;
};

export const setMusicVolume = (volume: number): void => {
  musicVolume = volume;
};

export const getSFXVolume = (): number => {
  return sfxVolume;
};

export const setSFXVolume = (volume: number): void => {
  sfxVolume = volume;
};
