import { FONT } from '../config';
import { getSFXVolume } from './sound';

export const createButton = (
  scene: Phaser.Scene,
  x: number,
  y: number,
  size: 'small' | 'medium' | 'large',
  type: 'square' | 'rectangle',
  text: string,
  callback: () => void,
  interactive: boolean = true,
): [Phaser.GameObjects.Text, Phaser.GameObjects.Sprite] => {
  const hoverSound = scene.sound.add('sfxUIButtonHover');
  const clickSound = scene.sound.add('sfxUIButtonClick');

  let fontSize: number;
  let spritesheet: string;
  if (size === 'small') {
    fontSize = 20;
    spritesheet = type === 'square' ? 'uiButtonSquareSmall' : 'uiButtonRectangleSmall';
  }
  if (size === 'medium') {
    fontSize = 25;
    spritesheet = type === 'square' ? 'uiButtonSquareMedium' : 'uiButtonRectangleMedium';
  }
  if (size === 'large') {
    fontSize = 30;
    spritesheet = type === 'square' ? 'uiButtonSquareLarge' : 'uiButtonRectangleLarge';
  }

  const buttonText = scene.add.text(x, y - 5, text, {
    fontFamily: FONT,
    fontSize: fontSize,
    color: '#ffffff',
    stroke: '#000000',
    strokeThickness: 4,
  });
  buttonText.setOrigin(0.5);
  buttonText.setAlpha(0.9);
  buttonText.setDepth(1);

  const button = scene.add.sprite(x, y, spritesheet);
  button.setOrigin(0.5);
  button.setFrame(0);
  button.setAlpha(0.9);
  if (interactive) {
    button.setInteractive();
  }
  button.on('pointerover', () => {
    button.setAlpha(1);
    buttonText.setAlpha(1);
    hoverSound.play({ volume: getSFXVolume() });
  });
  button.on('pointerout', () => {
    button.setAlpha(0.9);
    button.setFrame(0);
    buttonText.setAlpha(0.9);
    buttonText.y = y - 5;
  });
  button.on('pointerdown', () => {
    button.setFrame(1);
    buttonText.y = y + 5;
    clickSound.play({ volume: getSFXVolume() });
  });
  button.on('pointerup', () => {
    button.setFrame(0);
    buttonText.y = y - 5;
    callback();
  });

  return [buttonText, button];
};
